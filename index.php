<?php
require('vendor/autoload.php');

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Dibi as Dibi;
use \API\Drivers\OdbcOracleDriver;

$config['displayErrorDetails'] = true;
$config['db'] = [
	'driver' 	=> new OdbcOracleDriver(),
	'dsn' 		=> 'DSN=sba;DRIVER={Oracle in Oracle11g};SERVER=(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = fsboracle.fsb.miamioh.edu)(PORT = 1521)))(CONNECT_DATA=(SID=orcl)));',
	'username' 	=> 'nguyenkd',
	'password' 	=> 'm7ami',
];

$app = new \Slim\App(["settings" => $config]);
$container = $app->getContainer();

$container['db'] = function($c) {
	$db = $c['settings']['db'];
	$conn = new Dibi\Connection($db);
	return $conn;
};

require('middleware/Header.php');

$app->add(new Header());

require('routes/inventory.php');
require('routes/user.php');
require('routes/checkout.php');

// Run the API
$app->run();
