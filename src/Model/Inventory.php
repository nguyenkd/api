<?php
namespace API\Model;

class Inventory extends Model {
	public static $table = "INVENTORY";

	public static $primaryKey = "SERIAL_ID";

	protected $dataFields= [
		"SERIAL_ID", 
		"DEVICE_MODEL", 
		"DEVICE_TYPE",
		"RFID_ID", 
		"PURCHASE_DATE", 
		"WARRANTY_END_DATE",
		"COST",
		"ASSIGNMENT_STATUS"
	];

	protected $requiredDataFields = [
		"SERIAL_ID"
	];
}
