<?php

namespace API\Model;

class Checkout extends Model {
	public static $table = "CHECKOUT";

    public static $primaryKey = ["BANNER_ID", "SERIAL_ID"];

	protected $dataFields= [
        "BANNER_ID",
        "SERIAL_ID",
        "CHECKOUT_DATE",
        "RETURN_DATE"
    ];

    protected $requiredDataFields = ["BANNER_ID", "SERIAL_ID"];
}