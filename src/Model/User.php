<?php

namespace API\Model;

class User extends Model {
	public static $table = "USERS";

    public static $primaryKey = "BANNER_ID";

	protected $dataFields= [
        "BANNER_ID",
        "FNAME",
        "LNAME"
    ];

	protected $requiredDataFields = [
		"BANNER_ID"
	];
}