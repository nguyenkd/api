<?php
namespace API\Model;

abstract class Model {
	protected $attrs = array();

	public static $table;
	public static $primaryKey;

	protected $dataFields = [];
	protected $requiredDataFields = [];

	public function __construct(array $data) {
		$data = $this->formatData($data);

		foreach ($data as $key => $value) {
			$this->attrs[$key] = $value;
		}
	}

	public static function getTableName() {
		if (!static::isValidDataField(static::$table)) {
			//Get class name without namespaces
			return strtoupper(
				array_pop(
					explode('\\',static::class)
				)
			);
		} else {
			return strtoupper(static::$table);
		}
	}

	public static function getPrimaryKey() {
		if (!static::isValidDataField(static::$primaryKey)) {
			return strtoupper(static::getTableName() . "_ID");
		} else if (!is_array(static::$primaryKey) && count(static::$primaryKey) <= 1) {
			return strtoupper(static::$primaryKey);
		} else {
            $arr = array();
            
            foreach(static::$primaryKey as $key) {
                $arr[] = strtoupper($key);
            }
            
            return $arr;
        }
	}

	public function toAssocArray() {
		return $this->attrs;
	}

	/**
	 * Format data for correct number of fields
	 */
	public function formatData($data){
		$data = static::transformUpperKey($data);

		$newData = array();

		foreach ($this->dataFields as $key) {
			$isValidDataField = static::isValidDataField($data[$key]);

			if (in_array($key, $this->requiredDataFields) && !$isValidDataField) {
				throw new \RuntimeException("Missing Required Data Field");
			}

			$newData[$key] = $isValidDataField ? $data[$key] : null;
		}

		return $newData;
	}

	public static function transformUpperKey($data) {
		$transformedData = array();
		foreach ($data as $key => $value) {
			$transformedData[strtoupper($key)] = $value;
		}

		return $transformedData;
	}

	protected static function isValidDataField($dataField) {
		return isset($dataField) && !empty($dataField);
	}
}
