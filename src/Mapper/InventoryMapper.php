<?php

namespace API\Mapper;
// Method 1 to link models
use \API\Model\Inventory;
use \Dibi;

class InventoryMapper extends Mapper {
    // Method 1 to link to model
	protected $modelName = Inventory::class;
    // Method 2 to link to model, IF model in the API\Model namespace
	// protected $modelName = "Inventory";
	
	public function fetchByRFID($rfid) {
		$result = $this->db->query("SELECT * FROM {$this->table} WHERE RFID_ID = ?", $rfid);
		$result = $result->fetchAll();

		return $this->parseItems($result);
	}
}
