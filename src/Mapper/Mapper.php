<?php

namespace API\Mapper;

use \Dibi;

abstract class Mapper {
	protected $db;
	protected $table;
	protected $primaryKey;
	protected $modelName;

	public function __construct($db) {
		$this->db = $db;

		$modelName = $this->getModelPSR();
		$this->table = $modelName::getTableName();
		$this->primaryKey = $modelName::getPrimaryKey();
	}

	public function fetchAll($pageLimit = 100) {
		$result = $this->db->query("SELECT * FROM {$this->table} WHERE ROWNUM<={$pageLimit}");
		$result = $result->fetchAll();

		return $this->parseItems($result);
	}

	public function fetchByPrimaryKey($key) {
		$karr = null;
		if (is_string($key)) {
			if (count($this->primaryKey) > 1) {
				throw new \RuntimeException("Not enough Key");
			}
			$karr = [
				$this->primaryKey => $key,
			];
		} else {
			$karr = $key;
		};

		$result = $this->db->query("SELECT * FROM {$this->table} WHERE %and", $karr);
		$result = $result->fetchAll();

		return $this->parseItems($result);
	}

	public function getModelPSR() {
        if (strpos($this->modelName, "Model")) {
            return '\\' . $this->modelName; 
        } else {
            return '\\API\\Model\\' . $this->modelName;
        }
		
	}

	public function parseItems(array $result) {
		$devices = array();

		foreach ($result as $row) {
			$devices[] = $this->parseItem($row)->toAssocArray();
		}

		return $devices;
	}

	public function parseItem(Dibi\Row $row) {
		$class = $this->getModelPSR();
		$parameters = [$row->toArray()];

		return new $class(...$parameters);
	}
}
