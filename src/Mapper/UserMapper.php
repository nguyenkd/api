<?php

namespace API\Mapper;
// Method 1 to link models
use \API\Model\User;
use \Dibi;

class UserMapper extends Mapper {
    // Method 1 to link to model
	protected $modelName = User::class;
    // Method 2 to link to model, IF model in the API\Model namespace
    // protected $modelName = "Inventory";
}
