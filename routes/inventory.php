<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/inventory', function(Request $req, Response $res) {
	$mapper = new \API\Mapper\InventoryMapper($this->db);

	$device = $mapper->fetchAll();
	$res = $res->withJSON($device);

	return $res;
});

$app->get('/inventory/{serial_id}', function(Request $req, Response $res, $args) {
	$serial_id = $args['serial_id'];
	$mapper = new \API\Mapper\InventoryMapper($this->db);

	$device = $mapper->fetchByPrimaryKey($serial_id);
	$res = $res->withJSON($device);

    return $res;
});

$app->get('/inventory/rfid/{rfid}', function(Request $req, Response $res, $args) {
	$rfid = $args['rfid'];
	$mapper = new \API\Mapper\InventoryMapper($this->db);

	$device = $mapper->fetchByRFID($rfid);
	$res = $res->withJSON($device);

    return $res;
});