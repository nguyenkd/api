<?php 
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/user', function(Request $req, Response $res) {

	$mapper = new \API\Mapper\UserMapper($this->db);
	$user = $mapper->fetchAll();

	$res = $res->withJSON($user);

	return $res;
});

$app->get('/user/{banner_id}', function(Request $req, Response $res, $args) {
    $banner_id = $args['banner_id'];

	$mapper = new \API\Mapper\UserMapper($this->db);

	$user = $mapper->fetchByPrimaryKey($banner_id);
	$res = $res->withJSON($user);

	return $res;
});