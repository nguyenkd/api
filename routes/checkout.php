<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/checkout', function(Request $req, Response $res, $args) {
	$mapper = new \API\Mapper\CheckoutMapper($this->db);

	$device = $mapper->fetchAll();
	$res = $res->withJSON($device);

    return $res;
});

$app->get('/checkout/inventory/{serial_id}', function(Request $req, Response $res, $args) {
	$serial_id = $args['serial_id'];
	$mapper = new \API\Mapper\CheckoutMapper($this->db);
    
    $arr = [
        \API\Model\Inventory::$primaryKey => $serial_id,
    ];

	$device = $mapper->fetchByPrimaryKey($arr);
	$res = $res->withJSON($device);

    return $res;
});

$app->get('/checkout/user/{banner_id}', function(Request $req, Response $res, $args) {
    $banner_id = $args['banner_id'];
	$mapper = new \API\Mapper\CheckoutMapper($this->db);
    
    $arr = [
        \API\Model\User::$primaryKey => $banner_id
    ];

	$device = $mapper->fetchByPrimaryKey($arr);
	$res = $res->withJSON($device);

    return $res;
});

$app->get('/checkout/{serial_id}/{banner_id}', function(Request $req, Response $res, $args) {
	$serial_id = $args['serial_id'];
    $banner_id = $args['banner_id'];
	$mapper = new \API\Mapper\CheckoutMapper($this->db);
    
    $arr = [
        \API\Model\Inventory::$primaryKey => $serial_id,
        \API\Model\User::$primaryKey => $banner_id
    ];

	$device = $mapper->fetchByPrimaryKey($arr);
	$res = $res->withJSON($device);

    return $res;
});